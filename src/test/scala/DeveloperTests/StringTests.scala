package DeveloperTests

import DeveloperTests.Utils.FileUtils
import org.scalatest._

class StringTests extends FlatSpec with Matchers {

  "String" should "have a row level filter function to support multiple filters" in {

    val file = FileUtils.getBibleAsString()
    val res = null

    res.length should be (3087)

  }

  it should "be able to be chain line level filters" in {

    val file = FileUtils.getBibleAsString()
    val res = null

    res.length should be (2515)
  }

  it should "also have a word level filter" in {

    val file = FileUtils.getBibleAsString()
    val res = null

    res.length should be (929)
    res.distinct.length should be (287)
  }

  "word level filters" should "be chained to line level filters" in {

    val file = FileUtils.getBibleAsString()
    val res = null

    res.length should be (36)
    res.distinct.length should be (29)
  }
}
